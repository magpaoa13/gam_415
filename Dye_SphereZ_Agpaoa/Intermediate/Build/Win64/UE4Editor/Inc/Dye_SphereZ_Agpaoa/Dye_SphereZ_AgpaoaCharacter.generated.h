// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DYE_SPHEREZ_AGPAOA_Dye_SphereZ_AgpaoaCharacter_generated_h
#error "Dye_SphereZ_AgpaoaCharacter.generated.h already included, missing '#pragma once' in Dye_SphereZ_AgpaoaCharacter.h"
#endif
#define DYE_SPHEREZ_AGPAOA_Dye_SphereZ_AgpaoaCharacter_generated_h

#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_SPARSE_DATA
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_RPC_WRAPPERS
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADye_SphereZ_AgpaoaCharacter(); \
	friend struct Z_Construct_UClass_ADye_SphereZ_AgpaoaCharacter_Statics; \
public: \
	DECLARE_CLASS(ADye_SphereZ_AgpaoaCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Dye_SphereZ_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(ADye_SphereZ_AgpaoaCharacter)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesADye_SphereZ_AgpaoaCharacter(); \
	friend struct Z_Construct_UClass_ADye_SphereZ_AgpaoaCharacter_Statics; \
public: \
	DECLARE_CLASS(ADye_SphereZ_AgpaoaCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Dye_SphereZ_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(ADye_SphereZ_AgpaoaCharacter)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADye_SphereZ_AgpaoaCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADye_SphereZ_AgpaoaCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADye_SphereZ_AgpaoaCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADye_SphereZ_AgpaoaCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADye_SphereZ_AgpaoaCharacter(ADye_SphereZ_AgpaoaCharacter&&); \
	NO_API ADye_SphereZ_AgpaoaCharacter(const ADye_SphereZ_AgpaoaCharacter&); \
public:


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADye_SphereZ_AgpaoaCharacter(ADye_SphereZ_AgpaoaCharacter&&); \
	NO_API ADye_SphereZ_AgpaoaCharacter(const ADye_SphereZ_AgpaoaCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADye_SphereZ_AgpaoaCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADye_SphereZ_AgpaoaCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADye_SphereZ_AgpaoaCharacter)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaCharacter, L_MotionController); }


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_11_PROLOG
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_SPARSE_DATA \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_RPC_WRAPPERS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_INCLASS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_SPARSE_DATA \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DYE_SPHEREZ_AGPAOA_API UClass* StaticClass<class ADye_SphereZ_AgpaoaCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
