// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DYE_SPHEREZ_AGPAOA_Dye_SphereZ_AgpaoaHUD_generated_h
#error "Dye_SphereZ_AgpaoaHUD.generated.h already included, missing '#pragma once' in Dye_SphereZ_AgpaoaHUD.h"
#endif
#define DYE_SPHEREZ_AGPAOA_Dye_SphereZ_AgpaoaHUD_generated_h

#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_SPARSE_DATA
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_RPC_WRAPPERS
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADye_SphereZ_AgpaoaHUD(); \
	friend struct Z_Construct_UClass_ADye_SphereZ_AgpaoaHUD_Statics; \
public: \
	DECLARE_CLASS(ADye_SphereZ_AgpaoaHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Dye_SphereZ_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(ADye_SphereZ_AgpaoaHUD)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADye_SphereZ_AgpaoaHUD(); \
	friend struct Z_Construct_UClass_ADye_SphereZ_AgpaoaHUD_Statics; \
public: \
	DECLARE_CLASS(ADye_SphereZ_AgpaoaHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Dye_SphereZ_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(ADye_SphereZ_AgpaoaHUD)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADye_SphereZ_AgpaoaHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADye_SphereZ_AgpaoaHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADye_SphereZ_AgpaoaHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADye_SphereZ_AgpaoaHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADye_SphereZ_AgpaoaHUD(ADye_SphereZ_AgpaoaHUD&&); \
	NO_API ADye_SphereZ_AgpaoaHUD(const ADye_SphereZ_AgpaoaHUD&); \
public:


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADye_SphereZ_AgpaoaHUD(ADye_SphereZ_AgpaoaHUD&&); \
	NO_API ADye_SphereZ_AgpaoaHUD(const ADye_SphereZ_AgpaoaHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADye_SphereZ_AgpaoaHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADye_SphereZ_AgpaoaHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADye_SphereZ_AgpaoaHUD)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_9_PROLOG
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_SPARSE_DATA \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_RPC_WRAPPERS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_INCLASS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_SPARSE_DATA \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_INCLASS_NO_PURE_DECLS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DYE_SPHEREZ_AGPAOA_API UClass* StaticClass<class ADye_SphereZ_AgpaoaHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
