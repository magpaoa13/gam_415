// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DYE_SPHEREZ_AGPAOA_Dye_SphereZ_AgpaoaGameMode_generated_h
#error "Dye_SphereZ_AgpaoaGameMode.generated.h already included, missing '#pragma once' in Dye_SphereZ_AgpaoaGameMode.h"
#endif
#define DYE_SPHEREZ_AGPAOA_Dye_SphereZ_AgpaoaGameMode_generated_h

#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_SPARSE_DATA
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_RPC_WRAPPERS
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADye_SphereZ_AgpaoaGameMode(); \
	friend struct Z_Construct_UClass_ADye_SphereZ_AgpaoaGameMode_Statics; \
public: \
	DECLARE_CLASS(ADye_SphereZ_AgpaoaGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Dye_SphereZ_Agpaoa"), DYE_SPHEREZ_AGPAOA_API) \
	DECLARE_SERIALIZER(ADye_SphereZ_AgpaoaGameMode)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADye_SphereZ_AgpaoaGameMode(); \
	friend struct Z_Construct_UClass_ADye_SphereZ_AgpaoaGameMode_Statics; \
public: \
	DECLARE_CLASS(ADye_SphereZ_AgpaoaGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Dye_SphereZ_Agpaoa"), DYE_SPHEREZ_AGPAOA_API) \
	DECLARE_SERIALIZER(ADye_SphereZ_AgpaoaGameMode)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DYE_SPHEREZ_AGPAOA_API ADye_SphereZ_AgpaoaGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADye_SphereZ_AgpaoaGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DYE_SPHEREZ_AGPAOA_API, ADye_SphereZ_AgpaoaGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADye_SphereZ_AgpaoaGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DYE_SPHEREZ_AGPAOA_API ADye_SphereZ_AgpaoaGameMode(ADye_SphereZ_AgpaoaGameMode&&); \
	DYE_SPHEREZ_AGPAOA_API ADye_SphereZ_AgpaoaGameMode(const ADye_SphereZ_AgpaoaGameMode&); \
public:


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DYE_SPHEREZ_AGPAOA_API ADye_SphereZ_AgpaoaGameMode(ADye_SphereZ_AgpaoaGameMode&&); \
	DYE_SPHEREZ_AGPAOA_API ADye_SphereZ_AgpaoaGameMode(const ADye_SphereZ_AgpaoaGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DYE_SPHEREZ_AGPAOA_API, ADye_SphereZ_AgpaoaGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADye_SphereZ_AgpaoaGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADye_SphereZ_AgpaoaGameMode)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_9_PROLOG
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_SPARSE_DATA \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_RPC_WRAPPERS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_INCLASS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_SPARSE_DATA \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DYE_SPHEREZ_AGPAOA_API UClass* StaticClass<class ADye_SphereZ_AgpaoaGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
