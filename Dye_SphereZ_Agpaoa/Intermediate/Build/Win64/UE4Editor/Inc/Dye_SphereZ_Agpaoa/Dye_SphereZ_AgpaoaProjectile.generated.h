// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef DYE_SPHEREZ_AGPAOA_Dye_SphereZ_AgpaoaProjectile_generated_h
#error "Dye_SphereZ_AgpaoaProjectile.generated.h already included, missing '#pragma once' in Dye_SphereZ_AgpaoaProjectile.h"
#endif
#define DYE_SPHEREZ_AGPAOA_Dye_SphereZ_AgpaoaProjectile_generated_h

#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_SPARSE_DATA
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADye_SphereZ_AgpaoaProjectile(); \
	friend struct Z_Construct_UClass_ADye_SphereZ_AgpaoaProjectile_Statics; \
public: \
	DECLARE_CLASS(ADye_SphereZ_AgpaoaProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Dye_SphereZ_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(ADye_SphereZ_AgpaoaProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADye_SphereZ_AgpaoaProjectile(); \
	friend struct Z_Construct_UClass_ADye_SphereZ_AgpaoaProjectile_Statics; \
public: \
	DECLARE_CLASS(ADye_SphereZ_AgpaoaProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Dye_SphereZ_Agpaoa"), NO_API) \
	DECLARE_SERIALIZER(ADye_SphereZ_AgpaoaProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADye_SphereZ_AgpaoaProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADye_SphereZ_AgpaoaProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADye_SphereZ_AgpaoaProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADye_SphereZ_AgpaoaProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADye_SphereZ_AgpaoaProjectile(ADye_SphereZ_AgpaoaProjectile&&); \
	NO_API ADye_SphereZ_AgpaoaProjectile(const ADye_SphereZ_AgpaoaProjectile&); \
public:


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADye_SphereZ_AgpaoaProjectile(ADye_SphereZ_AgpaoaProjectile&&); \
	NO_API ADye_SphereZ_AgpaoaProjectile(const ADye_SphereZ_AgpaoaProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADye_SphereZ_AgpaoaProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADye_SphereZ_AgpaoaProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADye_SphereZ_AgpaoaProjectile)


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ADye_SphereZ_AgpaoaProjectile, ProjectileMovement); }


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_9_PROLOG
#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_SPARSE_DATA \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_RPC_WRAPPERS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_INCLASS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_SPARSE_DATA \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_INCLASS_NO_PURE_DECLS \
	Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DYE_SPHEREZ_AGPAOA_API UClass* StaticClass<class ADye_SphereZ_AgpaoaProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Dye_SphereZ_Agpaoa_Source_Dye_SphereZ_Agpaoa_Dye_SphereZ_AgpaoaProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
