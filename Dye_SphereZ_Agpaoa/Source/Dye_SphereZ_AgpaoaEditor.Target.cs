// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Dye_SphereZ_AgpaoaEditorTarget : TargetRules
{
	public Dye_SphereZ_AgpaoaEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("Dye_SphereZ_Agpaoa");
	}
}
