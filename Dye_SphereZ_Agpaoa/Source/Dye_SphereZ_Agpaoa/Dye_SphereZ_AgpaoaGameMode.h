// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Dye_SphereZ_AgpaoaGameMode.generated.h"

UCLASS(minimalapi)
class ADye_SphereZ_AgpaoaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADye_SphereZ_AgpaoaGameMode();
};



