// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Dye_SphereZ_AgpaoaGameMode.h"
#include "Dye_SphereZ_AgpaoaHUD.h"
#include "Dye_SphereZ_AgpaoaCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADye_SphereZ_AgpaoaGameMode::ADye_SphereZ_AgpaoaGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ADye_SphereZ_AgpaoaHUD::StaticClass();
}
