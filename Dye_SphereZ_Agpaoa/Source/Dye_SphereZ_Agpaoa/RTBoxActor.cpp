// Fill out your copyright notice in the Description page of Project Settings.


#include "RTBoxActor.h"

// Sets default values
ARTBoxActor::ARTBoxActor()
{
	mesh = CreateDefaultSubobject <URuntimeMeshComponentStatic>(TEXT("GeneratedMesh"));
	RootComponent = mesh;

}

// Called when the game starts or when spawned
void ARTBoxActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARTBoxActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARTBoxActor::GenerateBoxMesh() {
	TArray<FVector> Vertices;
	TArray<FVector> Normals;
	TArray<FRuntimeMeshTangent> Tangents;
	TArray<FVector2D> TextureCoordinates;
	TArray<int32> Triangles;
	TArray<FColor> Colors;
	CreateBoxMesh(FVector(200, 50, 50), Vertices, Triangles, Normals, TextureCoordinates, Tangents, Colors);
	mesh->CreateSectionFromComponents(0, 0, 0, Vertices, Triangles, Normals,
		TextureCoordinates, Colors, Tangents,
		ERuntimeMeshUpdateFrequency::Infrequent, true);

}