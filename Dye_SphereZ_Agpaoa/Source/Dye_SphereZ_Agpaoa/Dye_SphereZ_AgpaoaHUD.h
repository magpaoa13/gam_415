// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Dye_SphereZ_AgpaoaHUD.generated.h"

UCLASS()
class ADye_SphereZ_AgpaoaHUD : public AHUD
{
	GENERATED_BODY()

public:
	ADye_SphereZ_AgpaoaHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

