// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RTBoxActor.generated.h"
#include "Components/RuntimeMeshComponentStatic.h"

UCLASS()
class DYE_SPHEREZ_AGPAOA_API ARTBoxActor : public AActor
{
	GENERATED_BODY()
public:	
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius, TArray < FVector >& Vertices, TArray < int32 >& Triangles, TArray < FVector >& Normals, TArray < FVector2D >& UVs, TArray < FRuntimeMeshTangent >& Tangents, TArray < FColor >& Colors);


private:
	UPROPERTY(VisibleAnywhere)
	URuntimeMeshComponent* mesh;

public:	
	// Sets default values for this actor's properties
	ARTBoxActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
