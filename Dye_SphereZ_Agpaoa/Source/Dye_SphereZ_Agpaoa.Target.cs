// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Dye_SphereZ_AgpaoaTarget : TargetRules
{
	public Dye_SphereZ_AgpaoaTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("Dye_SphereZ_Agpaoa");
	}
}
